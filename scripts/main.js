window.bb = {};
window.bb_levels = {}; 
window.bb_utils = {}; 
window.bb_save = {}; 
window.bb_gui = {}; 
window.bb_net = {};

(function(bb, bb_levels, bb_utils, bb_save, bb_gui, bb_net){
	init = function(){
		if(ax && axd && axe && axi && axr){
			ax.width = 1366;
			ax.height = 768;
			ax.lockRes = true;
			ax.Init(30);
			ax.setBack("#000");
			ax.ignorFocus = true;
			//ax.debug = true;
			ax.direct.playScene("load_resources",{initial:true,section:1,scene:"init_resources"});
		} else {
			setTimeout(window.init, 1000);
		}
		
	}

	window.onload = function(){init()};
})(
	window.bb,
	window.bb_levels, 
	window.bb_utils, 
	window.bb_save, 
	window.bb_gui, 
	window.bb_net
	);
