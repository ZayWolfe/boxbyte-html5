(function(bb, bb_levels, bb_utils, bb_save, bb_gui, bb_net){
	
	bb_utils.copySimpleObj = function(obj1){
		var obj2 = {};
		for(key in obj1){
			obj2[key] = obj1[key];
		}
		return obj2;
	}
	
	bb_utils.returnColor = function(number){
		switch(number){
			case 1:
				return "B";
			case 2:
				return "G";
			case 3:
				return "R";
			default:
				return "B";
		}
	};
	bb_utils.pickColor = function(){
		var color = axu.randBet(1,6);
		if(color < 3){
			return 1;
		} else if(color < 5){
			return 2;
		} else {
			return 3;
		}
	}
	bb_utils.nextColor = function(num){
		num += 1;
		if(num > 3){
			num = 1;
		}
		return num;
	}
	
	
})(
	window.bb, 
	window.bb_levels, 
	window.bb_utils, 
	window.bb_save, 
	window.bb_gui, 
	window.bb_net
);
