(function(bb, bb_levels, bb_utils, bb_save, bb_gui, bb_net){
	bb_net.post = function(url,data,callback){
		var http = new XMLHttpRequest();
		http.open("POST", url, true);
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		if(callback)http.onreadystatechange = function(){callback(http)};
		http.send(data);
	};
	bb_net.get = function(url,data,callback){
		var http = new XMLHttpRequest();
		http.open("GET", url+"?"+data, true);
		if(callback)
			http.onreadystatechange = function(){callback(http);};
		http.send(null);
	};
})(
	window.bb, 
	window.bb_levels, 
	window.bb_utils, 
	window.bb_save, 
	window.bb_gui, 
	window.bb_net
	);