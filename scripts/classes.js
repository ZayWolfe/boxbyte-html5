//classes to Boxbyte
(function(bb, bb_levels, bb_utils, bb_save, bb_gui, bb_net){
	//a phantom byte that's only for the main menu background
	bb.DarkByte = function(x_point){//random showoff byte for main menu
		x_point = x_point || axu.randBet(-ax.halfWidth,ax.halfWidth);
		
		var byt = new ax.Actor({
			layer : 0,
			aniName : "byteDark",
			width : 20,
			height : 20,
			pos : [x_point, ax.halfHeight-2]
		});
		byt.vel[1] = -20;
		
		byt.addEvent("screenExit", function(){
			byt.kill();
			byt = null;
		})
	};
	//just the text the says "New Wave" with the count down
	bb.NewWaveText = function(parent){
		var newWave = new ax.Actor({
			width : 300,
			layer : "menu",
			fixed :true,
			pos : [20,-100],
			parent : parent
		});
		newWave.text.txt = "New Wave";
		newWave.text.color = "white";
		newWave.text.font = "Kubos";
		newWave.text.size = 40;
	};
	//the countdown in between waves
	bb.Counter = function(spawner){
		var count_actor = null;
		var number = 0;
		var self = this;
		
		//counter scripter
		var counter = new ax.Scripter();
		counter.loop = true;
		counter.cmd(1,function(){
			if((number -= 1) < 1){
				count_actor.kill();
				counter.kill();
				count_actor = null;
				if(spawner){
					spawner.spawn();
				}
			}
			axr.audio.count.play();
			if(count_actor)
				count_actor.text.txt = number+"";
		});
		
		//make actor and fire counter scripter
		this.init = function(num){
			number = num || 4;
			
			count_actor = new ax.Actor({
				layer : "menu",
				fixed : true,
				pos : [ax.halfWidth,ax.height/4],
				height : 60
			})
			count_actor.text.txt = number+"";
			count_actor.text.color = "white";
			count_actor.text.font = "Kubos";
			count_actor.text.size = 50;
			
			axr.audio.count.play();
			new bb.NewWaveText(count_actor);
			
			counter.fire();
		}
	}
	bb.Spawner = function(info_object, level_details){
		info_object = info_object || {};
		var byte_limit = level_details.byte_start_amount;
		var upgrade_count = 0;
		var spawn_count = 0;
		var countdown_started = false;
		var spawn_started = false;
		
		var counter = new bb.Counter(this);
		
		var update = new ax.Scripter();
		update.loop = true;
		update.cmd(1,function(){
			if(info_object.byte_count === 0 && !spawn_started){
				if(info_object.main_song){
					info_object.main_song.volume = 0.6;
				}
				axr.audio.newWave.stop();
				counter.init();
				spawn_started = true;
			}
		});
		update.fire();
		
		var spwn = new ax.Scripter()
		spwn.loop = true;
		spwn.cmd(0.2,function(){
			if(info_object.byte_count < byte_limit){
				(new bb.Byte(level_details.byte_spawn,level_details.path)).addEvent("cleared",function(){
					info_object.byte_count -= 1;
				});;
				info_object.byte_count += 1;
			} else {
				spawn_started = false;
				spwn.kill();
			}
		});
		
		this.spawn = function(){
			spawn_count += 1;
			if(spawn_count >= level_details.upgrade){
				spawn_count = 0;
				upgrade_count += 1;
				level_details.upgrade += level_details.upgrade_increase;
				byte_limit += level_details.wave_increment;
				if(byte_limit > level_details.max_bytes){
					byte_limit = level_details.max_bytes;
					console.log("max bytes!")
				}
			}
			if(upgrade_count >= level_details.max_upgrades){
				info_object.level_progress();
			}
			if(info_object.main_song){
				if(info_object.main_song.paused)
					info_object.main_song.play();
				info_object.main_song.volume = 1;
			}
			axr.audio.newWave.play();
			spwn.fire();
		}
	}
	//level code
	bb.Level = function(_obj){
		var info_object = null
		
		//outside vars
		this.path = _obj.path;
		this.start = _obj.start;
		
		_obj.max_upgrades = _obj.max_upgrades || 5;
		_obj.upgrade = _obj.upgrade || 2;
		_obj.upgrade_increase = _obj.upgrade_increase || 1;
		_obj.wave_increment = _obj.wave_increment || 3;
		_obj.byte_start_amount = _obj.byte_start_amount || 3;
		_obj.max_bytes = _obj.max_bytes || 80;
		_obj.mines = _obj.mines || 0;
		_obj.spawn_points = _obj.spawn_points || null;
		
		this.init = function(info_object){
			ax.level = undefined;
			ax.camera.edgeLock = true;
			ax.newWorld(0,0,[axr.images[_obj.imageName]]);
			new bb.ScoreBox(info_object);
			new bb.lifeBox(info_object);
		}
		this.getLevelDetails = function(){
			return _obj;
		}
	};
	bb.lifeBox = function(info_object){
		var life = new ax.Actor({
			layer:"menu",
			width:400,
			height:30,
			fixed:true,
			pos:[ax.width-200,20]
		});
		var ammount = info_object.getLife();
		
		life.addEvent("draw",function(){
			var start = 150;
			for(var i = 0; i < ammount; i++){
				axd.rect(true,life.pos[0]+start,life.pos[1],10,30,[0,150,0]);
				start -= 15;
			}
		});
		
		var update_scrp = new ax.Scripter();
		update_scrp.loop = true;
		update_scrp.cmd(1,function(){
			ammount = info_object.getLife();
		});
		update_scrp.fire();
		
	};
	bb.ScoreBox = function(info_object){
		var score = new ax.Actor({layer:"menu",width:400,fixed:true,pos:[ax.halfWidth+180,25]});
		score.text.color = "silver";
		score.text.font = "kubos";
		score.text.size = 20;
		score.text.txt = info_object.getScore()+"";
		
		var update_scrp = new ax.Scripter();
		update_scrp.loop = true;
		update_scrp.cmd(1,function(){
			score.text.txt = info_object.getScore()+"";
		});
		update_scrp.fire();
	}
	bb.Info = function(){
		var points_added = 0;
		var score = 0;
		var life = 10;
		
		this.byte_count = 0;
		this.level_progression = 0;
		
		this.addPoint = function(num){
			points_added += 1;
			score += 1;
			if(points_added === 100){
				points_added = 0;
			}
		};
		this.losePoint = function(num){
			score -= num;
		};
		this.getScore = function(){
			return score;
		};
		this.addLife = function(num){
			if(life < 30){
				life += num;
			}
		};
		this.loseLife = function(num){
			life -= 1;
			if(life <= 0){
				ax.direct.playScene("main_menu");
			}
		};
		this.getLife = function(){
			return life;
		}
	};
	//box code
	bb.Box = function(_obj){
		_obj = _obj || {};
		
		var box = new ax.Actor({
			width : 100,
			type : "box",
			height : 100,
			layer : 3,
			aniName : "boxB",
			pos : _obj.pos,
			path : _obj.path,
			pathCollisionRadius : 55,
			collision : 40
		});
		ax.camera.followDelay = 0.07;
		ax.camera.target = box;
		
		var color = 1;
		
		box.addEvent("moveout", function(data){
        box.angle = axu.findBounce(box.angle, axu.direction(data.wall[0],data.wall[1]));
        box.speed = box.speed * 1.2;
      });
      box.addEvent("collide",function(data){
      	var act = data.actor;
      	if(act.type === "byte"){
      		if(act.color === color){
      			_obj.inf.addPoint(100);
      			data.actor.kill()
      		} else {
      			_obj.inf.loseLife(1);
      		}
      	}
      });
		box.addEvent("update",function(){
			var x_movement = box.vel[0];
			var y_movement = box.vel[1];
			var key_down = false		
			
			if(axe.keys.LEFT.down){key_down = true;x_movement -= 10;}
			if(axe.keys.RIGHT.down){key_down = true;x_movement += 10;}
			if(axe.keys.UP.down){key_down = true;y_movement -= 10;}
			if(axe.keys.DOWN.down){key_down = true;y_movement += 10;}
			
			if(x_movement > 50)x_movement = 50;
			if(y_movement > 50)y_movement = 50;
			if(x_movement < -50)x_movement = -50;
			if(y_movement < -50)y_movement = -50;
			
			if(!key_down){
				x_movement = axu.round(x_movement*0.96*1000)/1000;
				y_movement = axu.round(y_movement*0.96*1000)/1000;
				if(Math.abs(x_movement) < 0.05)x_movement = 0;
				if(Math.abs(y_movement) < 0.05)y_movement = 0;
			}
			
			box.vel[0] = x_movement;
			box.vel[1] = y_movement;
			
			//change colors
			if(axe.keys.SPACE.up){
				color = bb_utils.nextColor(color);
				box.changeAni("box"+bb_utils.returnColor(color),box.currentFrame);
			}
			new bb.Trail([box.pos[0]-box.vel[0]/2,box.pos[1]-box.vel[1]/2],box.speed/2,box.angle,box.aniName,box.currentFrame);
		})
	};
	bb.Trail = function(pos,speed,angle, aniName, frame){
		var trail = new ax.Actor({
			layer : 2,
			pos : [pos[0],pos[1]],
			speed : speed,
			angle : angle+180,
			alpha : 0.5,
			aniName : aniName,
			currentFrame: frame
		});
		var fix_trail = true;
		trail.addEvent("update", function(){
			if(fix_trail){
				trail.pos[0]-=trail.vel[0]/2;
				trail.pos[1]-=trail.vel[1]/2;
				trail.currentFrame -= 1;
				fix_trail = false;
			}
			trail.alpha = axu.round((trail.alpha-0.1)*100)/100;
			trail.scale = axu.round((trail.scale-0.1)*100)/100;
			if(trail.alpha < 0.1)trail.kill();
		});
	};
	bb.Byte = function(pos,path,angle){
		var color = bb_utils.pickColor();
		var byt = new ax.Actor({
			layer : 1,
			type : "byte",
			aniName : "byte"+bb_utils.returnColor(color),
			width : 20,
			height : 20,
			pathCollisionRadius : 20,
			pos : pos,
			path : path,
			collision : 10
		});
		byt.color = color;
		
		byt.addEvent("moveout", function(data){
        byt.angle = axu.findBounce(byt.angle, axu.direction(data.wall[0],data.wall[1]));
      });
    
		
		byt.angle = angle || axu.randBet(0,359);
		byt.speed = axu.randBet(20,40);
		
		this.addEvent = byt.addEvent;
	}
	
	
})(
	window.bb, 
	window.bb_levels, 
	window.bb_utils, 
	window.bb_save, 
	window.bb_gui, 
	window.bb_net
	);
