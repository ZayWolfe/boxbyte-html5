//scenes of boxbyte
(function(bb, bb_levels, bb_utils, bb_save, bb_gui, bb_net){
	ax.direct.saveScene("init_resources",function(){
		//player/enemy sprites
		axr.loadSprites(
		 	["byteB","graphics/bytes/bytes-b.png",7,1],
		 	["byteG","graphics/bytes/bytes-g.png",7,1],
		 	["byteR","graphics/bytes/bytes-r.png",7,1],
		 	["byteDark","graphics/bytes/bytes-dark.png",7,1],
		 	["boxB","graphics/box/box-b.png",3,5],
		 	["boxG","graphics/box/box-g.png",3,5],
		 	["boxR","graphics/box/box-r.png",3,5],
		 	["e_bbB","graphics/enemies/bb-b.png",3,4],
		 	["e_mine","graphics/enemies/mine.png",6,15],
		 	//ui
		 	["logo_anim","graphics/ui/logo-sheet.png", 10, 9]
		 );
		 //gui
		 axr.loadImages(
		 	["gui_mode1","graphics/ui/mode1.png"],
		 	["gui_mode2","graphics/ui/mode2.png"],
		 	["gui_mode3","graphics/ui/mode3.png"],
		 	["gui_instruct","graphics/ui/instructions.png"],
		 	["gui_options","graphics/ui/options.png"],
		 	["gui_credits","graphics/ui/credits.png"],
		 	["gui_competition","graphics/ui/comp.png"]
		 );
		 //effects
		 axr.loadImages(
		 	["trailB", "graphics/effects/trailB.png"],
		 	["trailG", "graphics/effects/trailG.png"],
		 	["trailR", "graphics/effects/trailR.png"]
		 );
		 axr.loadSprites(
		 	["beat", "graphics/effects/beat.png",5,3]
		 );
		 //level images
		 axr.loadImages(
		 	["lvl1", "graphics/levels/level1.png"]
		 );
		 ///VIDEO
		 axr.loadVideos(
		 	["main_logo","video/omni", 576, 460, ["mp4","webm"]]
		 );
		 ////AUDIO
		 //music
		 axr.loadSounds(
		 	["main_menu","audio/music/ConFuze", ["mp3","ogg"],true],
		 	["music1","audio/music/AngryMod", ["mp3","ogg"],true]
		 );
		 //sfx
		 axr.loadSounds(
		 	["count","audio/fx/count", ["mp3","ogg"]],
		 	["newWave","audio/fx/newWave", ["mp3","ogg"]]
		 );
		 ax.direct.playScene("load_resources",{section:0,scene:"logos"});
	});

	ax.direct.saveScene("load_resources", function(data) {
		ax.clearScreen = true;
		data = data || {};
		data.section = data.section || 0;
		data.scene = data.scene || "main_menu";
		var ratio = [0, 1];
		var percentage = 0;
		var percentage_test;
		var display;
		
		//move to next scene script
		var move_scrp = new ax.Scripter();
		//visual representation
		if(!data.initial){
			label = new ax.Actor({pos:[10,-50],width:100});
			label.text.color = "#7777ff";
			label.text.weight = "bold";
			label.text.size = 16;
			label.text.txt = "loading...";
			
			new ax.Actor({pos:[-80,0],aniName:"boxB"});
			new ax.Actor({pos:[-15,0],aniName:"e_bbB"});
			new ax.Actor({pos:[22,0],aniName:"e_mine"});
			new ax.Actor({pos:[50,0],aniName:"byteB"});
			
			display = new ax.Actor({pos:[0,80],width:60});
			display.text.color = "#7777ff";
			display.text.weight = "bold";
			display.text.size = 16;
			display.text.txt = percentage+"%";
			
			move_scrp.cmd(3,function(){ax.direct.playScene(data.scene);});
		} else {
			move_scrp.cmd(0,function(){ax.direct.playScene(data.scene);});
			axr.loadSprites( 1,
			 	["byteB","graphics/bytes/bytes-b.png",7,1],
			 	["boxB","graphics/box/box-b.png",3,5],
			 	["e_bbB","graphics/enemies/bb-b.png",3,4],
			 	["e_mine","graphics/enemies/mine.png",6,15]
			 );
			 data.section = 1;
		}
		
		//load checking
		axe.addEvent("update", function() {
			console.log("test");
			if(display){
				percentage_test = axu.round((ratio[0]/ratio[1])*100);
				if(percentage_test > percentage){
					percentage = percentage_test;
				}
				display.text.txt = percentage+"%";
			};
			ratio = axr.load(data.section);
			if(ratio[0] === ratio[1]) {
				move_scrp.fire();
			}
		});
	});
	
	ax.direct.saveScene("logos",function(){
		axr.video.main_logo.addEvent("ended",function(){
			ax.direct.playScene("main_menu");
		});
		new ax.Actor({
			stamp : axr.video.main_logo.obj
		});
		setTimeout(function(){axr.video.main_logo.play();},500);
	});
	
	ax.direct.saveScene("main_menu", function(){
		ax.clearScreen = true;
		axr.resetAudio();
		axr.audio.main_menu.play();
		//design the menu
		var status_obj = new ax.Actor({
			pos : [0,ax.halfHeight-30],
			layer :1,
			width : ax.halfWidth,
			height : 40
		});
		status_obj.text.txt = "...";
		status_obj.text.font = "sans-serif";
		status_obj.text.color = "#ffffff";
		status_obj.text.size = 15;
		
		var main_menu = new bb_gui.Interface({
			layer:6,
			buttonTextSize : 50,
			buttonTextFont : "Kubos",
			buttonTextColor : "#ffffff",
			buttonTextOffset : [20,0]
		})
		main_menu.addMenus(
			{
				name : "main",
				target : [0,0],
				pos : [20,-50],
				statusObj : status_obj,
				statusMsg : "boxbyte" 
			}
		);
		
		
		main_menu.menus.main.addButtons(
			{
				stamp : axr.images.gui_mode1,
				pos : [-210,0],
				width : 400,
				height : 150,
				grow : true,
				blink : true,
				growLow : -0.05,
				growHigh : -0.02,
				buttonText : "ARENA",
				statusMsg : "Thanks for the laugh, you won't last a minute.",
				click : function(){ax.direct.playScene("arena_mode");}
			},
			{
				stamp : axr.images.gui_mode2,
				pos : [-210,150],
				width : 400,
				height : 150,
				grow : true,
				blink : true,
				growLow : -0.05,
				growHigh : -0.02,
				buttonText : "Survivor",
				statusMsg : "Can you reach the end? Just try."
			},
			{
				stamp : axr.images.gui_mode3,
				pos : [-210,300],
				width : 400,
				height : 150,
				blink : true,
				grow : true,
				growLow : -0.05,
				growHigh : -0.02,
				buttonText : "Insane",
				statusMsg : "Insane? Are you nuts?..."
			},
			{
				stamp : axr.images.gui_competition,
				pos : [85,150],
				width : 200,
				height : 470,
				blink : true,
				grow : true,
				growLow : -0.05,
				growHigh : -0.02,
				buttonTextSize : 45,
				buttonText : "Fight",
				statusMsg : "Play in competitions online and win prizes!"
			},
			{
				stamp : axr.images.gui_instruct,
				pos : [260,0],
				width : 150,
				height : 150,
				blink : true,
				grow : true,
				growLow : -0.05,
				growHigh : -0.02,
				buttonTextSize : 20,
				buttonText : "Info",
				statusMsg : "User manual included!"
			},
			{
				stamp : axr.images.gui_options,
				pos : [260,150],
				width : 150,
				height : 150,
				grow : true,
				blink : true,
				growLow : -0.05,
				growHigh : -0.02,
				buttonTextSize : 20,
				buttonText : "Options",
				statusMsg : "Tweek your options!"
			},
			{
				stamp : axr.images.gui_credits,
				pos : [260,300],
				width : 150,
				height : 150,
				blink : true,
				grow : true,
				growLow : -0.05,
				growHigh : -0.02,
				buttonTextSize : 20,
				buttonText : "Credits",
				statusMsg : "Boost our EGO page!"
			}
		);
		
		new ax.Actor({pos:[0,-250],layer:1,aniName:"logo_anim"});
		
		main_menu.init();
		
		var spawn_count = 30;
		var spawn_point = -ax.halfWidth;
		var spawn_gap = ax.width / 10;  
		axe.addEvent("update",function(){
			spawn_count -= 1;
			if(spawn_count === 0){
				spawn_count = 30;
				for(var x = 0; x < 11; x++){
					new bb.DarkByte(spawn_point);
					spawn_point += spawn_gap;
				}
				spawn_point = -ax.halfWidth;
			}
		})
	});
	
	//game arena mode code
	ax.direct.saveScene("arena_mode",function(){
		ax.clearScreen = false;
		axr.resetAudio();
		//setTimeout(function(){axr.audio.music1.play();},6000);
		var info_obj = new bb.Info();
		info_obj.main_song = axr.audio.music1;
		
		bb_levels._1.init(info_obj);
		
		new bb.Box({inf : info_obj,path : bb_levels._1.path});
		new bb.Spawner(info_obj, bb_levels._1.getLevelDetails());
		new ax.Actor({alpha:0.3,layer:0,aniName:"beat"})
	});
	
	ax.direct.saveScene("leaderboards",function(){
		var setScores = function(data){
			var place = 1;
			for(key in data){
				if(data.hasOwnProperty(key)){
					var score = data[key];
					if(score && score.name && score.score){
						new bb_gui.Label({
							width : 120,
							parent : score_parent,
							pos : [-400,start_y+=30],
							text : score.name,
							textColor : "white",
							textSize : 20
						});
						new bb_gui.Label({
							width : 60,
							parent : score_parent,
							pos : [-20,start_y],
							text : "<"+place+">",
							textColor : "white",
							textSize : 20
						});
						new bb_gui.Label({
							width : 100,
							parent : score_parent,
							pos : [430,start_y],
							text : score.score,
							textColor : "white",
							textSize : 20
						});
					}
					place += 1;
				}
			}
		};
		
		ax.clearScreen = true;
		var start_y = -ax.halfHeight+30;
		var score_parent = new ax.Actor({width:500,height:500});
		
		
		bb_net.get("../bb-server/score.php","",function(http){
			if (http.readyState==4 &&http.status==200)
				setScores(JSON.parse(http.responseText));
		});
	});

	
})(
	window.bb, 
	window.bb_levels, 
	window.bb_utils, 
	window.bb_save, 
	window.bb_gui, 
	window.bb_net
	);
