(function(bb, bb_levels, bb_utils, bb_save, bb_gui, bb_net){
	bb_gui.Label = function(_obj){
		var actor = new ax.Actor(_obj);
		if(_obj.text)actor.text.txt = _obj.text;
		if(_obj.textFont)actor.text.font = _obj.textFont;
		if(_obj.textColor)actor.text.color = _obj.textColor;
		if(_obj.textWeight)actor.text.weight = _obj.textWeight;
		if(_obj.textSize)actor.text.size = _obj.textSize;
		if(_obj.textOffset)actor.text.offset = _obj.textOffset;
	}
	bb_gui.Button = function(_obj, parent){
		_obj = _obj || {};
		
		var new_obj = bb_utils.copySimpleObj(_obj);
		new_obj.parent = parent;
		var but = new ax.Actor(new_obj);
		but.addEvent("cleared", function(){but = null;})
		
		//text stuff
		if(parent){
			but.text.txt = _obj.buttonText || parent.text.txt;
			but.text.font = _obj.buttonTextFont || parent.text.font;
			but.text.color = _obj.buttonTextColor || parent.text.color;
			but.text.weight = _obj.buttonTextWeight || parent.text.weight;
			but.text.size = _obj.buttonTextSize || parent.text.size;
			but.text.offset = _obj.buttonTextOffset || parent.text.offset;
		} else {
			if(_obj.buttonText)actor.text.txt = _obj.buttonText;
			if(_obj.buttonTextFont)actor.text.font = _obj.buttonTextFont;
			if(_obj.buttonTextColor)actor.text.color = _obj.buttonTextColor;
			if(_obj.buttonTextWeight)actor.text.weight = _obj.buttonTextWeight;
			if(_obj.buttonTextSize)actor.text.size = _obj.buttonTextSize;
			if(_obj.buttonTextOffset)actor.text.offset = _obj.buttonTextOffset;
		}
		
		//add blinking
		if(_obj.blink){
			but.alpha = _obj.blinkLow ||  0.7;
			but.addEvent("mouseEnter", function(){
				but.alpha = _obj.blinkHigh || 1.0;
			});
			but.addEvent("mouseExit", function(){
				but.alpha = _obj.blinkLow ||  0.7;
			});
		}
		if(_obj.grow){
			but.scale = _obj.growLow || -0.1;
			but.addEvent("mouseEnter", function(){
				but.scale = _obj.growHigh || 0;
			});
			but.addEvent("mouseExit", function(){
				but.scale = _obj.growLow || -0.1;
			});
		}
		if(_obj.change){
			var old_stamp = but.stamp;
			but.addEvent("mouseEnter", function(){
				but.stamp = _obj.change;
			});
			but.addEvent("mouseExit", function(){
				but.stamp = old_stamp;
			});
		}
		if(_obj.update){
			but.addEvent("update",_obj.update);
		}
		if(_obj.click){
			but.addEvent("leftUp",_obj.click);
		}
		if(_obj.statusObj){
			var msg = _obj.statusMsg || "";
			var old_msg = _obj.statusObj.text.txt;
			but.addEvent("update", function(){
				if(but.mhover){
					_obj.statusObj.text.txt = msg;
				}
			})
			but.addEvent("mouseExit", function(){
				_obj.statusObj.text.txt = old_msg;
			})
		}
		
	};
	bb_gui.Menu = function(_obj, parent){
		_obj = _obj || {};
		_obj.layer = _obj.layer || 6;
		_obj.pos = _obj.pos || [0,0];
		_obj.target = _obj.target || _obj.pos;
		
		var items = [];
		var actor = null;
		
		this.addButtons = function(){
			for(key in arguments){
				var button_init  = arguments[key];
				items.push(button_init);
			}
		}
		
		this.init = function(){
			actor = new ax.Actor(_obj);
			actor.addEvent("cleared",function(){actor = null;});
			
			//text settings
			if(parent){
				actor.text.txt = _obj.buttonText || parent.buttonText;
				actor.text.font = _obj.buttonTextFont || parent.buttonTextFont;
				actor.text.color = _obj.buttonTextColor || parent.buttonTextColor;
				actor.text.weight = _obj.buttonTextWeight || parent.buttonTextWeight;
				actor.text.size = _obj.buttonTextSize || parent.buttonTextSize;
				actor.text.offset = _obj.buttonTextOffset || parent.buttonTextOffset;
			} else {
				if(_obj.buttonText)actor.text.txt = _obj.buttonText;
				if(_obj.buttonTextFont)actor.text.font = _obj.buttonTextFont;
				if(_obj.buttonTextColor)actor.text.color = _obj.buttonTextColor;
				if(_obj.buttonTextWeight)actor.text.weight = _obj.buttonTextWeight;
				if(_obj.buttonTextSize)actor.text.size = _obj.buttonTextSize;
				if(_obj.buttonTextOffset)actor.text.offset = _obj.buttonTextOffset;
			}
			
			for(key in items){
				items[key].statusObj = items[key].statusObj || _obj.statusObj;
				items[key].statusMsg = items[key].statusMsg || _obj.statusMsg; 
				new  bb_gui.Button(items[key], actor);
			}
		};
		
	};
	bb_gui.Interface = function(_obj){
		//setting defaults for text
		_obj.buttonText = _obj.buttonText || "";
		_obj.buttonTextFont = _obj.buttonTextFont || "serif";
		_obj.buttonTextColor = _obj.buttonTextColor || "black";
		_obj.buttonTextWeight = _obj.buttonTextWeight || "";
		_obj.buttonTextSize = _obj.buttonTextSize || 12;
		_obj.buttonTextOffset = _obj.buttonTextOffset || [0,0];
		_obj.pos == _obj.pos || [0,0];
		_obj.layer = _obj.layer || 6;
		
		this.menus = {};
		
		this.addMenus = function(){
			for(key in arguments){
				var menu_init  = arguments[key];
				if(menu_init.name){
					menu_init.target = menu_init.target || menu_init.pos;
					menu_init.pos = menu_init.pos || _obj.pos;
					menu_init.layer = menu_init.layer || _obj.layer;
					var menu = new bb_gui.Menu(menu_init, _obj);
					this.menus[menu_init.name] = menu;
				} else {
					throw "All menus must have names";
				}
				
			}
		};
		
		this.init = function(){			
			for(key in this.menus){
				if(key in this.menus){
					var menu = this.menus[key]
					menu.init()
					if(menu.main){
						ax.camera.moveto(menu.target);
					}
				}
			}
		};
		
		this.makeMenuActive = function(name){
			if(this.menu[name]){
				ax.camera.moveto(this.menu[name].pos);
			} else {
				throw "Tried to moveto menu that doesn't exist";
			}
		};
		
	};
	
})(
	window.bb, 
	window.bb_levels, 
	window.bb_utils, 
	window.bb_save, 
	window.bb_gui, 
	window.bb_net
	);